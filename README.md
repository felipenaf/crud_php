# CRUD_PHP
Um CRUD(Create, Read, Update, Delete) que estou desenvolvendo.

CONTÉM NESTE ARQUIVO
---------------------   
 * INSTRUÇÕES
 * REQUERIMENTO
 * INSTALAÇÃO
 * CONFIGURAÇÃO

INSTRUÇÕES
------------
Para acessar o sistema é preciso importar o banco de dados e acessar via apache (as pastas devem estar no diretorio servidor do seu apache) e abrir a pasta _MVC.
--
Existem 5 pastas no documento, são elas __sql, _bean, _controller, _model, _view.
A pasta __sql, contem o arquivo que deve ser executado no MySql. Para importar, primeiramente excute a instrução "create database" para criar a base de dados, em seguida a instrução "create table" para poder criar a tabela filme.
--
As outras pastas são do modelo MVC (model, view e controller) que possuem o front-end e o back-end do site. Model, contem conexoes com banco de dados, que é o modelo do sistema. View, e todo o front-end e interação que o site tem. Controller é responsável por controlar as ações tomadas no sistema.
--
REQUERIMENTO
------------
Não há requerimentos.
--
INSTALAÇÃO
------------
Não há restrições de instalação
--
CONFIGURAÇÃO
-------------
Não há restrições de configuração